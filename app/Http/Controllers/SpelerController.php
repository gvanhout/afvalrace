<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SpelerController extends Controller
{
    public function index(){
        $spelers = DB::select('select * from spelers');
        $view = view('Spelers',["spelers"=>$spelers]);
        return $view;
    }
}
