<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ToernooiController extends Controller
{
    public function index(){
        $data = DB::select('select * from Toernooi, spelers, speler_regel');
        $view = view('Toernooien',["data"=>$data]);
        return $view;
    }
}
