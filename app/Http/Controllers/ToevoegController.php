<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ToevoegController extends Controller
{
    public function index(){
        $data = DB::select('select * from Toernooi, spelers, speler_regel');
        $view = view('Toevoegen',["data"=>$data]);
        return $view;
    }
}
