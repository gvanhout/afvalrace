<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Spelers extends Model
{
    protected $fillable = ['SpelerNaam', 'SpelerVereniging', 'SpelerTelefoonNummer', 'SpelerMail'];
    public $incrementing = true;
}
