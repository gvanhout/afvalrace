<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Toernooi extends Model
{



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'toernooi';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['ToernooiID', 'ToernooiNaam', 'ToernooiBeginDatum', 'ToernooiEindDatum', 'ToernooiPlaats', 'ToernooiAantal'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['ToernooiBeginDatum', 'ToernooiEindDatum'];

    public $incrementing = true;

}
