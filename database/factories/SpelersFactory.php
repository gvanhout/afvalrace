<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Spelers;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;

$factory->define(Spelers::class, function (Faker $faker) {
    foreach (range(1, 50) as $index) {
        DB::table('spelers')->insert([
            'SpelerNaam' => $faker->firstName,
            'SpelerVereniging' => $faker->languageCode,
            'SpelerTelefoonNummer' => $faker->numberBetween(1111111111, 9999999999),
            'SpelerMail' => $faker->companyEmail,
            'SpelerID' => $index
        ]);
    };
});
