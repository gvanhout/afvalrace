<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Generator as Faker;

class spelerseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Spelers::class, 50)->create()->each(function ($spelers) {
            $spelers->posts()->save(factory(App\Post::class)->make());
        });
    }
}
