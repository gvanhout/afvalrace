@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col">Naam</th>
                                    <th scope="col">Vereniging</th>
                                    <th scope="col">TelefoonNummer</th>
                                    <th scope="col">E-mail</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($spelers as $data)
                                    <tr>
                                        <th scope="row">{{$data->SpelerNaam}}</th>
                                        <td>{{$data->SpelerVereniging}}</td>
                                        <td>{{$data->SpelerTelefoonNummer}}</td>
                                        <td>{{$data->SpelerMail}}</td>
                                        <td>{{\Illuminate\Support\Facades\DB::select('select * from toernooi where $data->SpelerID = Spelers_SpelerID')}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection
